// console.log("Hello, General Kenobi");

// 3-4. Cube Calc + Template Literals
	
	const getCube = Math.pow(2, 3);
	console.log(`The cube of 2 is ${getCube}.`);

// 5-6. Address Array + Destructure
	// [ houseNumber, street, barangay, municipality, Province, zipCode ]

	let address = [7, "Heaven St", "Stargazer Heights", "Sector 7", "Midgar", "007"];

	// destructuring + temp literals
	let [houseNumber, street, barangay, municipality, Province, zipCode] = address;
	console.log(`Come grab a drink at Seventh Heaven! Find us at ${houseNumber} ${street}, ${barangay}, ${municipality}, ${Province}, ${zipCode}.`);

// 7-8. Animal as object + Destructure [TW: spider]
	// { name, species, weight, measurement }



	let animal = {
		name: "Tobey",
		species: "Chaco Golden Knee Tarantula",
		weight: "28.3 grams",
		measurement: "17.8 centimeters"
	}

	// destructuring + temp literals

	let {name, weight, species, measurement} = animal;
	console.log(`${name} was a ${species} who weighed roughly ${weight} and was ${measurement} small.`);

// 9-10. Number Array + forEach + Arrow Function
	/*
		Syntax:
			arrayName.forEach(function(indivElement){
				statement/s
			});

		num.forEach(function(x){console.log(x)})
	*/

	let num = [1, 2, 3, 4, 5]
	
	num.forEach((x) => console.log(x));

// 11. Reduce Array + Arrow function
	/*
		Syntax:
			arrayName.reduce(function(accumulator, currentValue){
				return operation/expression
			})

			num.reduce(function(x, y){
				return x+y
			})
	*/
	let reduced = num.reduce((a,b) => a+b);
	console.log(reduced);

// 12-13. Dog Maker via Constructor

	class Dog{
		constructor(name, age, breed){
			this.dogName = name;
			this.dogAge = age;
			this.dogBreed = breed;
		}
	}

	let pupper = new Dog("Maki", "1", "Shibeagle");
	console.log(pupper);